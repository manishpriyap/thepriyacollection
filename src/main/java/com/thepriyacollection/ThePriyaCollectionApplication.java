package com.thepriyacollection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThePriyaCollectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThePriyaCollectionApplication.class, args);
		System.out.println();
	}

}
